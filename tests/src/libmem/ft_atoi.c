/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awoimbee <awoimbee@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/02 15:30:13 by awoimbee          #+#    #+#             */
/*   Updated: 2020/06/02 15:32:42 by awoimbee         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft/ft_nb.h>
#include <check.h>

START_TEST (ft_atoi_general)
{
	const char *lol = "\n \t\n \f\r\v -2147483648 47483699";
	ck_assert(-2147483648 == ft_atoi(lol));
}
END_TEST

Suite	*build_suite_ft_atoi(void)
{
	Suite	*s = suite_create("ft_atoi");
	TCase	*tc = tcase_create("ft_atoi_general");
	suite_add_tcase(s, tc);
	tcase_add_test(tc, ft_atoi_general);
	return (s);
}
