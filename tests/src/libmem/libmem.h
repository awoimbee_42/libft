/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libmem.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awoimbee <awoimbee@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/20 23:30:28 by awoimbee          #+#    #+#             */
/*   Updated: 2020/06/02 15:32:52 by awoimbee         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TESTS_LIBMEM_H
# define TESTS_LIBMEM_H

Suite	*build_suite_ft_memcpy(void);
Suite	*build_suite_ft_atoi(void);

#endif
