/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libstr.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awoimbee <awoimbee@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/21 11:42:43 by awoimbee          #+#    #+#             */
/*   Updated: 2020/06/02 15:38:37 by awoimbee         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TESTS_LIBSTR_H
# define TESTS_LIBSTR_H

Suite	*build_suite_ft_strtrim(void);
Suite	*build_suite_ft_strrev(void);
Suite	*build_suite_ft_strcmp(void);

#endif
