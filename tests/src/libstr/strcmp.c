/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awoimbee <awoimbee@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/02 15:22:43 by awoimbee          #+#    #+#             */
/*   Updated: 2020/06/02 15:46:18 by awoimbee         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft/ft_str.h>
#include <stdlib.h>
#include <check.h>
#include <string.h>

START_TEST (ft_strcmp_test)
{
	const char *a = "abczpt\0ryithkgfh";
	const char *b = "abcatthh\0sfsdfsgfg";
	ck_assert_int_eq(strncmp(a, b, 99), ft_strncmp(a, b, 99));

	const char *c = "abc\0\0g";
	const char *d = "abc\0eg";
	ck_assert_int_eq(strcmp(c, d), ft_strcmp(c, d));
}
END_TEST

Suite	*build_suite_ft_strcmp(void)
{
	Suite	*s = suite_create("ft_strcmp");
	TCase	*tc = tcase_create("ft_strcmp_all");

	suite_add_tcase(s, tc);
	tcase_add_test(tc, ft_strcmp_test);
	return (s);
}
