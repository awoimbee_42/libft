/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awoimbee <awoimbee@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/02 15:22:43 by awoimbee          #+#    #+#             */
/*   Updated: 2020/06/02 15:29:07 by awoimbee         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft/ft_str.h>
#include <stdlib.h>
#include <check.h>
#include <string.h>

START_TEST (ft_strrev_test)
{
	const char *org = "qwerty";
	char rev[] = "ytrewq";
	ck_assert(strcmp(org, ft_strrev(rev)) == 0);
}
END_TEST

Suite	*build_suite_ft_strrev(void)
{
	Suite	*s = suite_create("ft_strrev");
	TCase	*tc = tcase_create("ft_strrev_all");

	suite_add_tcase(s, tc);
	tcase_add_test(tc, ft_strrev_test);
	return (s);
}
